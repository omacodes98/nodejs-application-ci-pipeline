# Build Nodejs Application Automation with Jenkins 

Set up a Continuous Integration Pipeline   

## Table of Contents

- [Project Description](#project-description)

- [Problem](#problem)

- [Technologies Used](#technologies-used)

- [Steps](#steps)

- [Installation](#installation)

- [Usage](#usage)

- [How to Contribute](#how-to-contribute)

- [Tests](#test)

- [Questions](#questions)

- [References](#references)

- [License](#license)

## Project Description

* Dockerize NodeJS app 

* Create a full pipeline 

* Manually deploy new Docker image on server 

* Create a Jenkins Shared Library 

## Problem 

Team members want to collaborate on a NodeJS application, where you list developers with their projects. So they ask me to set up a git repository for it.

Also, I think it's a good idea to add tests, to test that no one accidentally breaks the existing code.

Moreover, we all decide every change should be immediately built and pushed to the Docker repository, so everyone can access it right away.

For that they ask me to set up a continuous integration pipeline.

## Technologies Used 

* nodejs 

* Docker 

* GitLab

* Jenkins 

* Docker hub 

* Nexus 

## Steps 

Step 1: Create Dockerfile for Nodejs App

     touch Dockerfile

[Dockerfile](/images/01_created_dockerfile_for_app.png)

Step 2: Make a Cloud server on digitalocean for Jenkins 

[Jenkins server](/images/02_new_jenkins_server.png)

Step 3: Configure server firewall by opening port 8080 in which Jenkins listens on 

[Firewall](/images/03_firewall_configuration_to_allow_jenkins_listen.png)

Step 4: Ssh into server 

     ssh root@159.65.51.99

[ssh](/images/04_ssh_into_jenkins_server.png)

Step 5: Install docker on the server 

     apt install docker.io 

[Installing Docker](/images/05_installing_docker_on_server.png)

Step 6: Create a Jenkins Dockerfile on your own system which has docker and nodejs installed 

[Jenkins Dockerfile](/images/06_created_dockerfile_on_host.png)

Step 7: Login into nexus repo 

     docker login 142.93.37.150:8083


Step 8: Build an image with the jenkins dockerfile while including tag of nexus repo

     docker build -t 142.93.37.150:8083/Jenkins-docker-nodejs:1.0


Step 9: Push the Jenkins image to the nexus repo 

     docker push 142.93.37.150:8083/Jenkins-docker-nodejs:1.0

[Jenkins Image push](/images/07_image_pushed-to_nexus_repo)
[Nexus repo](/images/08_image_on_nexus_repo)

Step 10: Add insecure registry to daemon.json on server of the nexus repo because it is not secured 

     vim /etc/docker/daemon.json

[Adding Insecure R1](/images/09_adding_insecure_registry_in_daemonjson.png)
[Adding Insecure R2](/images/09_adding_insecure_registry_in_daemonjson_1.png)

Step 11: Restart docker to enable it to see insecure registry that has been added 

     systemctl restart docker 

[Restart Docker](/images/10_restarting_docker_to_add_insecure_registry.png)

Step 12: Pull Jenkins image from nexus onto the Jenkins server and run Jenkins container 

     docker pull 142.93.37.150:8083/Jenkins-docker-nodejs:1.0

[Pull Jenkins image](/images/11_pulling_docker_image_on_jenkins_server_from_nexus_server.png)

Step 13: Enter Jenkins container as root and change permission of Docker socket to enable you to execute docker commands in Jenkins container 

     docker exec -u 0 -it cbae15257ba /bin/bash
     chmod 666 /var/run/docker.sock 

[Docker Socker Permissions](/images/12_changing_permission_on_docker_socket_to_allow_us_execute_docker_commands.png)

Step 14: Check Jenkins UI on browser 

[Jenkins UI](/images/13_jenkins_ui.png)

Step 15: Get initial admin password in jenkins container 

     docker exec -it cbae15257ba /bin/bash 
     cat var/jenkins_home/secrets/InitialAdminpassword 

[Adminpassword](/images/14_get_initial_admin_passworrd.png)

Step 16: Carry out Jenkins Initial installations 

[Jenkins Initial Installation](/images/15_jenkins_initial_installation.png)

Step 17: Create a multibranch pipeline and configure it to have access to the source 

[Multibranch source configuration](/images/16_creating_multibranch_pipline_and_configuring_it_to_have_access_to_source.png)

Step 18: Create Jenkinsfile in Gitlab in which multibranch is connected this is because jenkins will be looking for a jenkinsfile to build the the pipeline 

Step 19: Install webhook plugin on Jenkins to  auto trigger pipeline 

[Webhook plugin](/images/17_downloading_webhook_for_auto_trigger.png)

Step 20: Configure webhook on multibranch

[Multibranch webhook config](/images/18_configuring_webhook_trigger_on_multibranch.png)

Step 22: Go onto your gitlab repo and configure webhook on there to cause an automatic pipeline build on push 

[Gitlab Webhook](/images/19_webhook_configured_on_gitlab.png)

Step 23: Test to see if pipeline is triggered automatically and is building 

[Auto trigger](/images/20_triggered_automatically.png)
[infinite loop](/images/21_infinite_loop_triggered.png)
[Jenkins Commit](/images/22_jenkins_commit.png)

Step 24: It worked however, due to adding Jenkins commit and push logic into the jenkins file it has created an inifinite loop. This is caused because Jenkins keeps pushing new version bump to the repo. To stop this we will need to install a plugin called ignore commit strategy 

[Ignore commit strategy](/images/23_installing_ignore_commit-strategy.png)

Step 25: Configure multibranch by adding ignore commit strategy plugin onto it and giving it jenkins email address that has been put in the Jenkinsfile for the jenkins commit logic. This causes Jenkins to ignore auto triggering through any commit with that certain email address.

[Add ICS on Multibranch](/images/24_configuring_multibranch_by_adding_ignore_committer_strategy.png)

Step 26: Push some changes to trigger Pipeline and test if there is still an infinite loop 

     git add .
     git commit -m "testing if infinite loop has been stopped"
     git push 

[Push to trigger pipeline](/images/25_pushing_to_trigger_pipeline.png)
[Infinit Loop Stopped](/images/26_infinite_loop_stopped.png)
[logs](/images/27_logs.png)
[Docker hub](/images/28_docker_hub-repo.png)

Step 27: Deploy manually on server

     docker run -d -p 3000:3000 omacodes98/nodejs-app.1.0.2-29

[Deploy app manually](/images/29_deploying_manually_on_server.png)
[Application UI](/images/30_application_ui.png)

Step 28: Create a Jenkins Shared Library 

[Jenkins Shared Library Creation](/images/31_creating_library.png)

  --> https://gitlab.com/omacodes98/second-jenkins-shared-library.git <--

Step 29: Make Jenkins Shared Library available globally on Jenkins 

[JSL globally available](/images/32_making_library_available_globally_on_jenkins.png)

Step 30: Edit Jenkins file and add function to call groovy scripts from shared library 

Step 31: Create JSL branch to test shared library before putting it on main branch 

[JSL branch](/images/33_created_jsl_branch_on_gitlab.png)

Step 32: Test if shared library is working effectively by pushing to gitlab repo to triggering auto pipeline build 

[JSL Pushing to trigger](/images/34_pushing_to_trigger_pipeline.png)
[Shared Library works](/images/35_shared_library_working_in_pipeline.png)
[JSL Jenkins commit](/images/36_jenkins_commit_after_shared_library.png)

## Installation

Run $ apt install docker.io 
Run $ apt install nodejs 

## Usage 

Run $ node server.js

## How to Contribute

1. Clone the repo using $ git clone git@gitlab.com:omacodes98/nodejs-application-ci-pipeline.git

2. Create a new branch $ git checkout -b your name 

3. Make Changes and test 

4. Submit a pull request with description for review

## Tests

Test were ran using npm test.

## Questions

Feel free to contact me for further questions via: 

Gitlab: https://gitlab.com/omacodes98

Email: omacodes98@gmail.com

## References

https://gitlab.com/omacodes98/nodejs-application-ci-pipeline

https://gitlab.com/omacodes98/second-jenkins-shared-library

## License

The MIT License 

For more informaation you can click the link below:

https://opensource.org/licenses/MIT

Copyright (c) 2022 Omatsola Beji.

